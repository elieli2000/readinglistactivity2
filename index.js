// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:


    function grade() {

    var inputGrade = prompt('Enter your grade to check your letter grade');
    var grade=getGrade(inputGrade);
    alert(grade);
    }
    function getGrade(inputGrade) {
        if (inputGrade >= 90) {
            return 'Congratulations! Your quarterly average is '+ inputGrade +' Advanced';
        }
        if (inputGrade >= 80) {
            return 'Congratulations! Your quarterly average is '+ inputGrade +' Developing';
        }
        if (inputGrade >= 75) {
            return 'Congratulations! Your quarterly average is '+ inputGrade +' Beginner';
        }
        if (inputGrade <= 74) {
            return 'Failed';
        }
        return 'Failed';
    }

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

for (var x=1; x<=300; x++) {
        if (x === 0) {
                console.log(x +  " - even");
        }
        else if (x % 2 === 0) {
                console.log(x + " - even");   
        }
        else {
                console.log(x + " - odd");
        }
}


/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, 
	description, skills(object which will contain 3 uinique skills). 
	Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/


const hero = {
skills:{}
}

hero.heroName = prompt('Hero name: ');

hero.origin = prompt('Hero origin: ');

hero.description = prompt('Hero description: ');

hero.skills.skill1 = prompt('Skill 1: ');

hero.skills.skill2 = prompt('Skill 2: ');

hero.skills.skill3 = prompt('Skill 3: ');

hero.skills.skill4 = prompt('Skill 4: ');

console.log(JSON.stringify(hero));
		  